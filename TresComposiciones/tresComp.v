(** Autor: Luis B. Lluis LL11
  script que desarrolla las propiedades básicas de una 
  3-composición*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From Coq Require Import Lia.
From CompLL11 Require Import Defs_tresComp . 

(** 1. Composiciones por valores*)

(*Estructura de ternas de naturales*)
Print natter.
(* funciones proyección*)
Print fst3.
Print snd3.
Print trd3.
Locate "( x , y , z )".
Check (1,2, 3).

(*Relación sobre ternas para saber si forman una 3-composición de n*)
Print ComV3.

(*  PONER EJEMPLOS CANONICOS DE 3-COMPOSICIONES*) 

(** 2. Composiciones por separadores*)
(* Relación sobre parejas para saber si son separadores*)
Print Sep3.
(*Ejemplos canónicos de separadores *)
Example zz_Sep3: forall n:nat, Sep3 (0,0).
Proof.
  intros.
  unfold Sep3.
  simpl.
  constructor. 
Qed.

Example zn_Sep3: forall n:nat,  Sep3 (0,n).
Proof.
  intros.
  unfold Sep3.
  simpl.
  apply le_0_n.
Qed.

Example nn_Sep3: forall n:nat, Sep3 (n,n).
Proof.
  intros.
  unfold Sep3.
  simpl.
  constructor.
Qed.


(* Relación de 3-composición de n*)
Print Com3.
(* Composiciones canónicas*)
Example zz_Com3: forall n:nat, Com3 (0,0) n.
Proof.
  intros. 
  split.
  apply zz_Sep3. apply n.
  simpl.
  apply le_0_n.
Qed.

Example zn_Com3: forall n:nat, Com3 (0,n) n.
Proof.
  intros.
  split.
  apply zn_Sep3.
  simpl.
  constructor.
Qed.

Example nn_Com3: forall n:nat, Com3 (n,n) n.
Proof.
  intros.
  split.
  apply nn_Sep3.
  simpl.
  constructor.
Qed.

(** 3. Funciones de equivalencia de representaciones de composiciones*)

(* Funciones de traducción de una representación a la otra*)
Print toVal3C.
Print toSep3C.


Proposition equiv_Val_Sep_3C: forall (n:nat) (P: nat*nat),
  Sep3 P -> toSep3C (toVal3C P n) = P.
Proof.
  intros.
  destruct P as [x y].
  unfold toVal3C.
  simpl.
  unfold toSep3C.
  simpl.
  assert (x + (y - x)=y).
  unfold Sep3 in H.
  simpl in H.
  lia.
  rewrite H0.
  reflexivity.
Qed.

Proposition equiv_Sep_Val_3C:  forall (n:nat)(T:natter),
  (ComV3 T n) ->(toVal3C(toSep3C T) n)= T.
Proof.
  intros.
  destruct T as [a b c].
  unfold toSep3C.
  simpl.
  unfold toVal3C.
  simpl.
  assert (a + b - a=b).
  lia.
  assert (n - (a + b)= c ).
  + unfold ComV3 in H.
    simpl in H.
    lia.
  + rewrite H0.
    rewrite H1.
    reflexivity.
Qed.



(** 4. Orden de separadores*)
(* DEf orma semejante buscamos definir un orden 
entre las composiciones. Para esto requerimos un orden en los
separadores. Para las 2-composiciones no hizo falta este paso
pues en los eparadores se representaban con un natural y el
orden de estos era el mismo al verse como separadores. 
En este caso usaremos un orden lexicográgico leyendo de 
derecha a izquierda. Mostraremos que este orden es un COTO reflexivo*)

(*oreden lexicográfico leyendo de derecha a izquierda*)
Print leSep3.


Proposition leSep3_refl: forall A: nat*nat,
  Sep3 A-> leSep3 A A.
Proof.
  intros.
  destruct A as [x y].
  split.
  assumption.
  split.
  assumption.
  right.
  simpl.
  split.
  reflexivity.
  constructor.
Qed.

Proposition leSep3_antisym: forall A B:nat*nat,
    (leSep3 A B)->(leSep3 B A) -> A = B.
Proof.
  intros.
  destruct A as [a1 a2].
  destruct B as [b1 b2].
  unfold leSep3 in *.
  simpl in *.
  destruct H.
  destruct H1.
  destruct H0.
  destruct H3.
  destruct H2,H4.
  + eapply lt_trans in H2.
    2: { apply H4. }
    contradict H2.
    apply lt_irrefl.
  + destruct H4.
    rewrite H4 in H2.
    contradict H2.
    apply lt_irrefl.
  + destruct H2.
    rewrite H2 in H4.
    contradict H4.
    apply lt_irrefl.
  + destruct H4.
    destruct H2.
    eapply le_antisymm in H6.
    2: { assumption. }
    rewrite H6.
    rewrite H4.
    reflexivity.
Qed.

Proposition leSep3_trans: forall A B C:nat*nat,
    (leSep3 A B)-> (leSep3 B C)->(leSep3 A C).
Proof.
  intros.
  destruct A as [a1 a2].
  destruct B as [b1 b2].
  destruct C as [c1 c2].
  unfold leSep3 in*.
  simpl in *.
  destruct H.
  destruct H1.
  destruct H0.
  destruct H3.
  split. assumption.
  split. assumption.
  destruct H2 ,H4.
  + left. 
    eapply lt_trans.
    apply H2. assumption.
  + left. 
    destruct H4.
    rewrite <-H4.
    assumption.
  + left.
    destruct H2.
    rewrite H2.
    assumption.
  + right.
    destruct H2.
    destruct H4.
    split.
    - transitivity b2.
      assumption. assumption.
    - eapply le_trans.
      apply H5.
      assumption.
Qed.

Proposition leSep3_dich: forall A B: nat *nat,
  Sep3 A-> Sep3 B -> 
  (leSep3 A B) \/ (leSep3 B A).
Proof.
  intros.
  destruct A as [a1 a2].
  destruct B as [b1 b2].
  unfold Sep3 in *.
  unfold leSep3 in *.
  simpl in *.
  pose proof lt_trichotomy.
  specialize H1 with (n:= a2)(m:= b2).
  destruct H1.
  + left.
    split. assumption.
    split. assumption.
    left. assumption.
  + destruct H1.
  2: { right.
       split. assumption.
       split. assumption.
        left. assumption. }
  pose proof le_ge_cases.
  specialize H2 with (n:= a1)(m:= b1).
  destruct H2.
  - left.
    split. assumption.
    split. assumption.
    right.
    split. 
    assumption. assumption.
  - right.
    split. assumption.
    split. assumption. 
    right. split.
    symmetry in H1.
    assumption.
    assumption.
Qed.


(* PROBAR BUEN ORDEN EN  Sep3*)



(** 5. Orden en composiciones *)
(*Veamos que el orden es una retricción a los separadores que forman una
composición y por tanto todas las propiedades del orden se herdan*)
Print ordC3.

Proposition ordC3_refl: forall (A: nat*nat)(n:nat), 
  Com3 A n -> (ordC3 A A n).
Proof.
  intros.
  destruct A as [x y].
  unfold ordC3.
  split.
  apply leSep3_refl.
  unfold Com3 in H.
  destruct H.
  assumption.
  split.
  assumption.
  assumption.
Qed.

Proposition ordC3_antisym: forall (A B: nat*nat)(n:nat),
  (ordC3 A B n) -> (ordC3 B A n)
  -> A = B.
Proof.
  intros.
  unfold ordC3 in *.
  destruct H.
  destruct H0.
  destruct H1.
  unfold Com3 in *.
  destruct H1. 
  destruct H3.
  apply leSep3_antisym.
  assumption.
  assumption.
Qed. 

Proposition ordC3_trans: forall (A B C: nat*nat)(n:nat),
  (ordC3 A B n) -> (ordC3 B C n)-> (ordC3 A C n).
Proof.
  intros.
  unfold ordC3 in *.
  destruct H.
  destruct H1.
  destruct H0.
  destruct H3.
  split.
  eapply leSep3_trans.
  apply H.
  assumption.
  split. assumption.
  assumption.
Qed.

Proposition ordC3_dich: forall (A B: nat*nat)(n:nat),
  (Com3 A n)-> (Com3 B n)-> 
    ((ordC3 A B n)\/(ordC3 B A n)).
Proof.
  intros.
  pose proof leSep3_dich.
  specialize H1 with (A:= A)(B:= B).
  unfold Com3 in *.
  destruct H. 
  destruct H0.
  
  destruct H1.
  + assumption.
  + assumption.
  + left. unfold ordC3.
    split. 
    assumption.
    split. split.
    assumption. assumption.
    split. 
    assumption. assumption.
  + right. unfold ordC3.
    split. 
    assumption.
    split. split. 
    assumption. assumption.
    split.
    assumption. assumption.
Qed.

(* Probar que es buen orden *)



(* Veamos que para n fijo este orden tiene mínimo y mnáximo. *)
Proposition ordC3_Min: forall (P:nat*nat) (n:nat),
  (Com3 P n) -> (ordC3 (0,0) P n).
Proof.
  intros.
  split.
  destruct P as [x y].
  unfold leSep3.
  simpl in *.
  split. apply zz_Sep3. apply n.
  split. destruct H. assumption.
  pose proof  eq_0_gt_0_cases.
  specialize H0 with (n:= y).
  destruct H0.
  + right.
    split. 
    - symmetry. assumption.
    - apply le_0_n.
  + left.
    assumption.
  + split.
    apply zz_Com3.
    assumption. 
Qed.
  
  
Proposition ordC3_Max: forall (P:nat*nat) (n:nat),
  (Com3 P n) -> (ordC3 P (n,n) n).
Proof.
  intros.
  split.
  destruct P as [a b].
  + unfold leSep3.
    simpl.
    destruct H.
    unfold Sep3 in H.
    simpl in *.
    apply lt_eq_cases in H0.
    destruct H0.
    - split. assumption.
      split. apply nn_Sep3.
      left.
      assumption.
    - split. assumption.
      split. apply nn_Sep3.
      right.
      split.
      assumption.
      rewrite <-H0.
      assumption.
  + split. 
    assumption.
    apply nn_Com3.
Qed.




(** 6. HasNext y Next*)
(* De nuevo, definamos la estructura iterativa de las composiciones.*)

Print hasNext3C.

Proposition hasNext3C_Com3: forall (P: nat*nat) (n:nat), 
  hasNext3C P n -> Com3 P n.
Proof.
  intros.
  unfold Com3.
  split.
  unfold hasNext3C in H.
  destruct H.
  assumption.
  unfold hasNext3C in H.
  destruct H.
  pose proof le_succ_diag_r.
  specialize H1 with (n:= (snd P)).
  eapply le_trans.
  apply H1.
  assumption.
Qed.


Proposition upbound_Com3: forall (n:nat)(P1 P2 : nat*nat),
  Com3 P2 n-> (leSep3 P1 P2) -> Com3 P1 n.
Proof.
  intros.
  split.
  unfold leSep3 in H0.
  destruct H0.
  assumption.
  destruct H0.
  destruct H1.
  
  destruct H2.
  unfold Sep3 in *.
  unfold Com3 in *.
  destruct H.
  eapply le_trans.
  2: {apply H3. }
  apply lt_le_incl.
  assumption.
  destruct H2.
  unfold Com3 in H.
  destruct H.
  rewrite H2.
  assumption.
Qed.

Print next3C.

Lemma next3C_disyuctive: forall (P: nat * nat),
  (next3C P = (0, S (snd P))) \/ 
  (next3C P=(S (fst P), snd P)).
Proof.
Admitted.
(*   intros.
  destruct P as [a b].
  simpl.
  assert (a =? b \/ )
  decide equality.
  assert  *)
  
  
(*   Hint Resolve ltb_reflect leb_reflect eqb_reflect : bdestruct. *)
  
Proposition next3C_Com2: forall (P:nat*nat) (n: nat),
  hasNext3C P n -> Com3 (next3C P) n.
Proof.
  intros.
  destruct P as [a b].
  destruct H.
  unfold Sep3 in H0.
  simpl in H.
  simpl in H0.
  simpl.
  split.
(*(*   + bdestruct (a =? b).*)
  pose proof lt_trichotomy.
  specialize H1 with (n:= a)(m:=b).
(*   split. 
  destruct H1.*)*)
  destruct H0.
  + simpl. 
     assert ((a =? a) = true).
     apply eqb_eq. reflexivity. 
     rewrite H0.
     unfold Sep3.  simpl.
     apply le_0_n.
  + assert ((a=? S m )=false).
    Locate "=?".
    apply Lt.le_lt_n_Sm in H0 as T.
    apply compare_lt_iff in T.
Admitted.
(*     rewrite T. 
    simpl in T.
    Search Gt.
    
    
      
  
  
  
  
  
  
  + assert ((a =? b) = false).
    -admit.
    - unfold Sep3.  simpl.
      rewrite H1.
      simpl.
      apply H0.
  + destruct H0.
    - unfold Sep3.
      simpl.
      rewrite H0.
      assert (b =? b = true). admit.
      rewrite H1.
      simpl.
      apply le_0_n.
    - unfold hasNext3C in H.
      simpl in H.
      destruct H.
      unfold Sep3 in H1.
      simpl in H1.
      eapply le_lt_trans in H1.
      2: { apply H0. }
      contradict H1.
      apply lt_irrefl.
  + destruct H0.
    - simpl. 
      assert ((a =? b) = false). admit.
      rewrite H1.
      simpl. 
      unfold hasNext3C in H.
      destruct H.
      simpl in H.
      eapply le_trans.
      apply le_succ_diag_r.
      assumption.
    - destruct H0.
      * rewrite H0. simpl.
        assert (b =? b = true). admit.
        rewrite H1.
        simpl.
        unfold hasNext3C in H.
        simpl in H.
        destruct H.
        assumption.
     *  contradict H0.
        unfold hasNext3C in H.
        simpl in H.
        destruct H.
        unfold Sep3 in H0.
        simpl in H0.
        intro.
        eapply le_lt_trans in H0.
        2: { apply H1. }
        apply lt_irrefl in H0.
        assumption.)
Admitted.
 *)

Proposition mono_next3C: forall (n :nat)(P: nat * nat),
  Com3 P n-> hasNext3C P n-> ordC3 P (next3C P) n.
Proof.
  intros.
  split.
  destruct P as [a b].
Admitted.
(*  apply le_succ_diag_r .
  split.
  assumption.
  apply next2C_Com2.
  assumption.
Qed. *)

