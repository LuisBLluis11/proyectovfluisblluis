(** Autor: Luis B. Lluis LL11
  script de definiciones de una 3-composición*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
Inductive natter : Type :=
| ter (x : nat) (y : nat) (z: nat).


Notation "( x , y , z )" := (ter x y z).
(* Notation "X * Y * Z" := (prod3 X Y Z) (at level 70, b at next level): type_scope. *)

Definition fst3  (p:natter):nat :=
  match p with (x ,y ,z) => x end.
Definition snd3 (p:natter):nat:= 
  match p with (x, y, z) => y end.
Definition trd3 (p:natter): nat:= 
  match p with (x, y, z) => z end.
  
  
(*Para una 3-composición se usará una terna de valores
  que sumen n. A esta representación se le denominará
  por valor. Se empleará la representación por separador
  para dotar de un orden a estas composciones.*)
Definition ComV3 (T :natter) (n:nat) := 
            fst3 T + snd3 T + trd3 T  =n. 


(* Funciones de traducción de una representación a la otra*)
Definition toVal3C (P: nat*nat) (n: nat): natter := 
  (fst P,snd P - fst P ,n-snd P).

Definition toSep3C (T : natter): (nat*nat):=
  (fst3 T , fst3 T + snd3 T).
  
(* Estructura de datos que representará los separadores
con los cuales se generará una 3 composición.
Para generar una 3-composición se necesitarán de dos separadores.
Estos separadores no pueden sobrepasar a n y para garantizar
unicidad de representación se pide que los separadores no se 
sobrepasen uno al otro.*)
Definition Sep3 (P : nat*nat):Prop := fst P <= snd P.

(* Definimos una 3-composición como un separador que
no sobre pasa a n*)

Definition Com3 (Sep: nat * nat) n :=
        (Sep3 Sep )/\ (snd Sep <= n).


(*oreden lexicográfico leyendo de derecha a izquierda*)
Definition leSep3 (A B: nat*nat):= 
  Sep3 A /\ Sep3 B /\
  (snd A < snd B \/ (snd A =snd B /\ fst A <= fst B)).



(*Veamos que el orden es una retricción a los separadores que forman una
composición y por tanto todas las propiedades del orden se herdan*)
 
Definition ordC3 A B n := 
  leSep3 A B /\ (Com3 A n) /\ (Com3 B n) .

(* De nuevo, definamos la estructura iterativa de las composiciones.*)

Definition hasNext3C (A: nat* nat)(n:nat):=
    S(snd A)<= n /\ Sep3 A.



Definition next3C (P:nat*nat): nat*nat  := 
  match P with 
    | (a,b)=> 
    
    (*match a with 
                | b => (0,S b) 
                | _ => (S a,b) 
                end
    end.
    *)
    
    if a =? b  then (0,S b) else (S a,b)
    end. 


