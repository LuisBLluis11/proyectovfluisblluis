(** Autor: Luis Felipe Benítez Lluis
	Número: 3
	*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_des .  
From CompLL11 Require Import Props_des . 


(* Propiedades de orden para listas descendientes
   para mostrar que es COTO reflexivo*)
Proposition leDes_refl: forall A: list nat,
  Des A->  A <=Des A.
Proof. 
  intros.
  induction H.
  + constructor.
  + apply leDesHH.
    reflexivity.
    assumption.
Qed.


Proposition leDes_antisym: forall A B:list nat,
    Des A-> Des B-> 
    (A <=Des B)->(B <=Des A) -> A = B.
Proof.
  intros.
  induction H1.
    + inversion H2.
      - reflexivity.
      - reflexivity.
    + constructor.
    
    + inversion H2.
      - contradict H1.
        rewrite H5.
        apply lt_irrefl. 
      - contradict H1.
        apply lt_asymm.
        assumption.
      - contradict H1.
        rewrite H6.
        apply lt_irrefl.
    + inversion H.
      inversion H0.
      apply IHleDes in H6.
      rewrite H6.
      rewrite H1.
      reflexivity.
      assumption.
      inversion H2.
      ++ constructor.
      ++ contradict H13.
         rewrite H1. 
         apply lt_irrefl.
      ++ assumption.
Qed.

Proposition leDes_tr: forall B C:list nat,
    Des B-> Des C->(B <=Des C)->
    forall A: list nat,Des A->(A <=Des B)->(A <=Des C).
Proof.
  do 5 intro.
  induction H1. 
  + intros. inversion H2.
    - constructor.
    - constructor.
  + intros. assumption.
  + intros. inversion H3.
    - constructor.
    - constructor. assumption.
    - assert (h0 < h2).
      eapply lt_trans. apply H6. assumption.
      constructor. assumption.
    - rewrite H7. constructor.
      assumption.
  + intros.
    induction A.
    constructor.
    inversion H4.
    apply leDesHH.
    assumption.
    assumption.
    apply leDesTT.
    rewrite <-H1.
    assumption. 
    apply leDesHH.
    rewrite H8.
    assumption.
    apply IHleDes.
    - apply Des_tail in H. simpl in H.
      assumption.
    - apply Des_tail in H0. simpl in H0.
      assumption.
    - apply Des_tail in H3. simpl in H3.
      assumption.
    - assumption.
Qed.

Proposition leDes_trans: forall A B C:list nat,
    Des A-> Des B->Des C->
    (A <=Des B)->(B <=Des C)->(A <=Des C).
Proof. 
  intros.
  eapply leDes_tr.
  apply H0.
  assumption.
  assumption.
  assumption.
  assumption.
Qed.
  

Proposition leDes_dich: forall A B: list nat,
  Des A-> Des B -> 
  (A <=Des B) \/ (B <=Des A).
Proof.
  intros.
  generalize dependent B.
  induction A.
  + intros. left. constructor.
  + intros. induction B.
    right. constructor.
    pose proof lt_trichotomy as T.
    specialize T with (n:= a)(m:= a0).
    destruct T.
    ++ left. apply leDesTT. assumption.
    ++ destruct H1.
      - apply Des_tail in H. simpl in H.
        assert (forall B : list nat, Des B -> A <=Des B \/ B <=Des A) as T.
        { apply IHA. apply H. }
        specialize T with (B:=B).
        apply Des_tail in H0. simpl in H0.
        apply T in H0.
        destruct H0.
        -- left. apply leDesHH.
           assumption.
           assumption.
        -- right. apply leDesHH.
           symmetry.
           assumption.
           assumption.
      -  right. apply leDesTT. assumption.
Qed.

(*Propiedades de manejo de head*)
Proposition des_head_mono: forall (A B :list nat)(a b :nat),
  (a::A) <=Des b::B -> a<=b.
Proof.
  intros.
  inversion H.
  constructor.
  subst.
  apply lt_le_incl.
  assumption.
  subst.
  constructor.
Qed.

Proposition des_head_mono2: forall (A B: list nat),
  A<=Des B->  (hd 0 A) <= (hd 0 B ).
Proof.
intros.
inversion H.
simpl.
apply le_0_l.
constructor.
subst.
apply des_head_mono in H.
simpl.
assumption.
simpl.
rewrite H0.
constructor.
Qed.

