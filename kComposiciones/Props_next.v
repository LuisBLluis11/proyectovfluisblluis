(** Autor: Luis Felipe Benítez Lluis
	Número: 10
	*)
Require Import List.
Require Import PeanoNat.
Require Import Bool .
Import ListNotations.
Import Nat.

From CompLL11 Require Import Defs_des .
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 

From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_sep .
From CompLL11 Require Import Props_Ordsep . 

From CompLL11 Require Import Defs_aux .
From CompLL11 Require Import Props_aux .
From CompLL11 Require Import Defs_next .


(** 1- Propiedades de hasNext*)

Proposition hasNext_case: forall (l:list nat)(n:nat),
   hasNext l n =true -> 
    hd n l < n \/ (hasNext (tl l) n=true/\ hd n l = n).
Proof.
  intro.
  destruct l.
  + intros.
    simpl.
    simpl in H.
    discriminate H.
  + intros.
    inversion H.
(*     inversion H0. *)
    apply orb_true_iff in H1.
    destruct H1.
    left.
    simpl.
    apply ltb_lt.
    assumption.
    right.
    simpl.
    apply  andb_true_iff  in H0.
    destruct H0.
    rewrite H1.
    apply eqb_eq in H0.
    rewrite H0.
    split.
    2: reflexivity.
    rewrite eqb_refl.
    rewrite orb_true_r.
    subst.
    assumption.
Qed.


Proposition hasNext_head: forall (l:list nat )(n:nat),
  hasNext l n=true -> hd 0 l <= n.
Proof.
intro.
destruct l.
+ intros.
  simpl in H.
  discriminate H.
+ intros.
  simpl.
  apply hasNext_case in H.
  destruct H.
  simpl in H.
  apply lt_le_incl.
  assumption.
  destruct H.
  simpl in H0.
  rewrite H0.
  constructor.
Qed.
Proposition zeros_hasNext: forall (k n:nat),
  0<k ->0< n ->hasNext (zeros k) n = true.
Proof.
intros.
induction k.
inversion H.
destruct n.
inversion H0.
simpl.
reflexivity.
Qed.

Proposition enn_hasntNext: forall (k n:nat),
  hasNext (enn n k) n = false.
Proof. 
intro.
induction k.
intros.
simpl.
reflexivity.
intros.
simpl.
rewrite ltb_irrefl.
rewrite orb_false_l.
rewrite eqb_refl.
rewrite andb_true_l.
rewrite IHk.
reflexivity.
Qed.

Proposition des_hasntNext_enn:forall (l:list nat),
 hasNext l (hd 0 l)=false -> Des l ->
    l = enn(hd 0 l) (length l).
Proof.
intros.
induction l.
simpl.
reflexivity.
simpl.
assert (l = enn a (length l)).
+ simpl in H.
  apply orb_false_elim in H.
  destruct H.
  rewrite eqb_refl in H1.
  rewrite andb_true_l in H1.
  clear H.
  simpl in H1.
  inversion H0.
  subst.
  destruct l.
  - simpl.
    reflexivity.
  - simpl in IHl.
    simpl in H1.
    simpl in H4.
    apply lt_eq_cases in H4.
    destruct H4.
      {
      apply ltb_lt in H.
      rewrite H in H1.
      rewrite orb_true_l in H1.
      inversion H1. }
      subst.
      simpl.
      apply IHl.
      assumption.
      assumption.
+ rewrite <-H1.
  reflexivity.
Qed.
      
Compute hasNext [3;2;1] 2.

Proposition des_hasntNext_head: forall (l:list nat)
  (r:nat),l<>[]-> hasNext l r = false -> 
   hd 0 l<= r -> Des l ->  r = hd 0 l .
Proof.
intros.
destruct l.
contradict H.
reflexivity.
simpl.
simpl in H1.
apply lt_eq_cases in H1.
destruct H1.
simpl in H0.
apply ltb_lt in H1.
rewrite H1 in H0.
rewrite orb_true_l in H0.
inversion H0.
symmetry.
assumption.
Qed.


(** 2.- Propiedades de Next *)

Proposition next_cases: forall (a:nat)(A:list nat),
  Next (a::A)=a::(Next A)\/ Next (a::A)=(S a)::zeros(length A).
Proof.
intros.
simpl.
remember (hasNext A a) as b.
destruct b.
+ simpl. left. reflexivity.
+ simpl. right. reflexivity.
Qed.

Proposition next_cases2: forall (A:list nat),
  Next A = [] \/Next A =(hd 0 A)::(Next (tl A))\/ Next A=(S (hd 0 A))::zeros(length (tl A)).
Proof.
intros.
destruct A.
simpl.
left.
reflexivity.
right.
apply next_cases.
Qed.

Proposition next_head_bound: forall  (l:list nat)(n d:nat),  
 hasNext l n=true -> hd d (Next l)<= n.
Proof.
intro.
induction l.
+ intros.
  simpl in H.
  inversion H.
+ intros.
  apply hasNext_case in H.
  destruct H.
  simpl.
  destruct (hasNext l a).
  ++ simpl.
     simpl in H.
     apply lt_le_incl.
     assumption.
  ++ simpl. 
     simpl in H.
     apply Lt.lt_le_S.
     assumption.
  ++ destruct (hasNext (a::l) n).
     simpl in H.
     destruct H.
     simpl.
     rewrite <-H0 in H.
     rewrite H.
     simpl.
     rewrite H0.
     constructor. 
     destruct H.
     simpl.
     simpl in H.
     simpl in H0.
     rewrite <-H0 in H.
     rewrite H.
     simpl.
     subst.
     constructor.
Qed.

Proposition next_head_bound2:forall  (l:list nat)(n d:nat),  
 hd d l <n -> hd d (Next l)<= n.
Proof.
intros.
induction l.
simpl in H.
simpl.
apply lt_le_incl.
assumption.
- simpl.
  remember (hasNext l a) as b.
  destruct b. 
  simpl in H.
  -- simpl. 
     apply lt_le_incl.
     assumption.
  -- simpl.
     apply Lt.lt_le_S.
     assumption.
Qed.




Proposition next_length: forall  (l:list nat),
 length (Next l) = length l.
Proof.
intro.
induction l.
+ intros.
  simpl.
  reflexivity.
+ intros.
  simpl.
  destruct (hasNext l a).
  - simpl.
    rewrite (IHl).
    reflexivity.
  - simpl.
    rewrite zeros_length.
    reflexivity.
Qed.
    
    


Proposition next_is_Des: forall  (l:list nat),  Des (Next l ).
Proof.
intro.
induction l.
simpl.
constructor.
+ remember (hasNext l a) as b.
  destruct b.
  - simpl.
    rewrite <-Heqb.
    symmetry in Heqb.
    apply hasNext_case in Heqb.
    destruct Heqb.
    apply next_head_bound2 in H.
    constructor.
    assumption.
    remember (Next l) as L.
    destruct L.
    { simpl. apply le_0_l. }
    { simpl. simpl in H. assumption. }
    destruct H. 
    constructor. assumption.
    destruct l.
    simpl in H.
    inversion H.
    simpl.
    simpl in H.
    simpl in H0.
    rewrite H0.
    rewrite H.
    simpl.
    constructor.
  - simpl.
    rewrite <-Heqb.
    constructor.
    apply zeros_is_Des.
    destruct l.
    ++ simpl.
       apply le_0_l.
    ++ simpl. 
       apply le_0_l.
Qed.

Proposition next_is_Sep_H: forall  (l:list nat)(n k:nat),  
length l = k-> hd 0 l< n-> Sep (Next l) n k.
Proof.
intros.
destruct l.
rewrite<- H.
simpl.
apply (nil_is_Sep n).
simpl in H0.
constructor.
apply (next_is_Des (n0::l)).
split.
apply next_head_bound2.
simpl.
assumption.
rewrite next_length.
assumption.
Qed.

Proposition next_is_Sep_hN: forall  (l:list nat)(n k:nat),  
  length l = k-> hasNext l n=true-> Sep (Next l) n k.
Proof.
intros.
split.
apply next_is_Des.
split.
apply next_head_bound.
assumption.
rewrite (next_length l).
assumption.
Qed.

     

Proposition next_leDes: forall (l:list nat),  
    l <=Des (Next l).
Proof.
intros.
induction l.
simpl.
constructor.
simpl.
remember (hasNext l a) as b.
destruct b.
+ apply leDesHH.
  reflexivity.
  assumption.
+ constructor.
  apply lt_succ_r.
  constructor.
Qed.


Proposition next_leSep_hN: forall (l:list nat)(n k:nat),
  Sep l n k->hasNext l n = true-> leSep l (Next l) n k.
Proof.
intros.
split.
assumption.
split.
apply next_is_Sep_hN.
inversion H.
destruct H2.
assumption.
assumption.
apply next_leDes.
Qed.

Proposition next_leSep_H: forall (l:list nat)(n k:nat),
  Sep l n k->hd n l< n-> leSep l (Next l) n k.
Proof.
intros.
assert (hasNext l n=true).
destruct l.
simpl in H0.
contradict H0.
apply lt_irrefl.
simpl.
simpl in H0.
apply ltb_lt in H0.
rewrite H0.
simpl.
reflexivity.
apply next_leSep_hN.
assumption.
assumption.
Qed.
Compute Next [3;4].
Compute Next [3; 5].
Compute Next [3].

Proposition next_is_nil_iff_nil: forall (A:list nat),
  Next A = [] <-> A = [].
Proof.
induction A.
simpl.
apply iff_refl.
split.
+ intros.
  pose proof (next_length (a::A)).
  rewrite H in H0.
  simpl in H0.
  inversion H0.
+ intros.
  inversion H.
Qed.

Proposition next_is_zeros_iff_nil: forall (A:list nat)(k:nat),
  Next A = zeros(k)<-> (k=0 /\ A = []).
Proof.
intro.
induction A.
intros.
split.
+ simpl. 
  intros.
  split.
  { symmetry in H.
    apply zeros_is_nil_iff_nil in H.
    assumption. }  
  reflexivity.
+  intros.
   destruct H.
   rewrite H.
   simpl.
   reflexivity.
+  intros.
   split.    
  { intros.  
    pose proof (zeros_length k) as ZL.
    pose proof (next_length  (a::A))as NL.
    rewrite <-H in ZL.
    rewrite NL in ZL.
    simpl in ZL.
    rewrite <-ZL in H.
    pose proof (next_cases a A) as W1.
    pose proof  (le_0_l a) as W2.
    apply lt_eq_cases in W2.
    destruct W1,W2.
    - rewrite H0 in H.
      simpl in H.
      inversion H.
      subst.
      inversion H1.
    - rewrite H0 in H.
      simpl in H.
      inversion H.
      apply IHA in H4.
      destruct H4.
      simpl in H0.
      rewrite H4 in H0.
      simpl in H0.
      inversion H0.
      subst.
      inversion H6.
    - rewrite H0 in H.
      simpl in H.
      inversion H.
    - rewrite H0 in H.
      simpl in H.
      inversion H. }
   { intros.
     destruct H.
      subst.
      inversion H0. }
Qed.

    



Proposition next_not_idem: forall (l:list nat),
   l <> [] -> l <> Next l.
Proof.
intros.
induction l.
contradict H.
reflexivity.
remember (hasNext l a) as b.
destruct b.
+ simpl.
  rewrite <-Heqb.
  destruct l.
  simpl  in Heqb.
  inversion Heqb. 
  intro.
  inversion H0.
  apply IHl.
  intro.
  inversion H1.
  exact H2.
+ simpl.
  rewrite <- Heqb.
  intro.
  inversion H0.
  contradict H2.
  apply neq_succ_diag_r.
Qed.

Proposition next_is_sucZer_iff_tlhanstNext:
  forall (a:nat)(A:list nat),  Des (a::A)-> 
  (Next (a::A)= (S a)::zeros(length A)<->
    A = enn a (length A)).
Proof.
simpl.
intros.
remember (hasNext A a) as b.
split.
+ intros.
  destruct b.
  inversion H0.
  contradict H2.
  apply n_Sn.
  assert (a = hd 0 (a::A)).
  reflexivity.
(*   rewrite H1.*)
  symmetry in Heqb.
  destruct A.
  {  simpl.
    reflexivity. }
  { remember (n::A) as B.
  apply des_hasntNext_head in Heqb as W.
  -  simpl. rewrite W.
     rewrite W in Heqb.
     apply des_hasntNext_enn.
     -- assumption.
     -- inversion H.
        assumption.
  - rewrite HeqB.
    intro.
    inversion H2.
  - rewrite HeqB.
    simpl.
    inversion H.
    subst.
    simpl in H5.
    assumption.
  - inversion H.
    assumption.  }
+ intros.
  rewrite H0 in Heqb.
  rewrite enn_hasntNext in Heqb.
  subst.
  reflexivity.
Qed.
    
Compute Next [3; 5].
Compute Next [3; 6].
Compute Next [3; 7].


Compute Next [7; 5].
Compute Next [7; 6].
Compute Next [7; 7].
Proposition next_sandwich: forall (A B:list nat),
  Des A -> Des B -> length A = length B -> 
    A<=Des B -> B<=Des Next A -> B=A\/B= Next A.
Proof.
intros.
induction H2.
simpl in H1.
symmetry in H1.
apply length_zero_iff_nil  in H1.
left.
rewrite H1.
reflexivity.
left. reflexivity.
+ simpl.
Admitted.

Proposition next_inj1:forall (k:nat)(A B:list nat),
  Des A->Des B ->length A = k -> length B =k->
   Next A =Next B-> A =B.
Proof.
intro.
induction k.
intros A B DA DB LA LB NAB.
apply length_zero_iff_nil in LA.
apply length_zero_iff_nil in LB.
subst.
reflexivity.
+ intros A B DA DB LA LB NAB.
   destruct A,B.
  {reflexivity.  }
  { simpl in LA.
    inversion LA. }
  { simpl in LB.
    inversion LB. }
    simpl in LB;inversion LB.
    simpl in LA;inversion LA.
  assert(Next A = Next B -> A = B) as IH.
  { apply IHk.
    inversion DA.
    assumption.
    inversion DB.
    assumption.
    assumption.
    assumption. }
    clear IHk.
    subst.
  pose proof (next_cases n A) as CA.
  pose proof (next_cases n0 B) as CB.
  destruct CA,CB .
  { rewrite <-NAB in H0.
    rewrite H in H0.
    inversion H0.
    apply IH in H4.
    rewrite H4.
    reflexivity. }
  { rewrite <-NAB in H0.
    rewrite H in H0.
    inversion H0.
    rewrite <-H1 in H4.
    apply next_is_zeros_iff_nil in H4.
    destruct H4.
    rewrite H4 in NAB.
    rewrite H2 in H1.
    symmetry in H1.
    apply length_zero_iff_nil in H1.
    rewrite H1 in NAB.
    simpl in NAB.
    inversion NAB.
    rewrite H6 in H3.
    contradict H3.
    apply n_Sn. }
  { rewrite <-NAB in H0.
    rewrite H in H0.
    inversion H0.
    rewrite H1 in H4.
    symmetry in H4.
    apply next_is_zeros_iff_nil in H4.
    destruct H4.
    rewrite H4 in NAB.
    rewrite H2 in H1.
    apply length_zero_iff_nil in H1.
    rewrite H1 in NAB.
    simpl in NAB.
    inversion NAB.
    rewrite H6 in H3.
    symmetry in H3.
    contradict H3.
    apply n_Sn. }
  { apply next_is_sucZer_iff_tlhanstNext in H as WA.
    apply next_is_sucZer_iff_tlhanstNext in H0 as WB.
    rewrite <-NAB in H0.
    rewrite H in H0.
    inversion H0.
    subst.
    rewrite WA.
    rewrite WB.
    rewrite H1.
    reflexivity.
    assumption.
    assumption. }
Qed.    

Proposition next_inj:forall (A B:list nat),
  Des A->Des B ->length A = length B ->
   Next A =Next B-> A =B.
Proof.
intros.
remember (length  A) as k.
apply (next_inj1 k).
assumption.
assumption.
rewrite Heqk.
reflexivity.
rewrite H1.
reflexivity.
assumption.
Qed.






