(** Autor: Luis Felipe Benítez Lluis
	Número: 8
	*)
Require Import List.
Require Import PeanoNat.
(* Require Import Bool . *)
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_des .
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 


From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_sep .
From CompLL11 Require Import Props_Ordsep . 
From CompLL11 Require Import Defs_aux . 
From Coq Require Import Lia.




(** 1.- Propiedades de Suma *)
Proposition Sum_concat: forall (A B:list nat),
  Sum (A++B)= Sum A + Sum B.
Proof.
intros.
induction A.
simpl.
reflexivity.
simpl.
rewrite IHA.
lia.
Qed.

Proposition Sum_ToCom: forall (A:list nat)(n:nat),
  Sum (n::(A++[0]))= n+Sum(A).
Proof.
induction A.
- simpl.
  reflexivity.
- intros.
  simpl.
  rewrite <-(IHA a).
  simpl.
  lia.
Qed.

Proposition Sum_gt_head: forall (A:list nat),
  hd 0 A <= Sum A.
Proof.
induction A.
simpl.
constructor.
simpl.
apply le_add_r.
Qed.
Proposition Sum_gt_last: forall (A:list nat),
  last A 0 <= Sum A.
Proof.
induction A.
simpl.
constructor.
destruct A.
simpl.
apply le_add_r.
remember (n::A) as B.
simpl.
remember (last B 0) as X.
rewrite HeqB.
rewrite <-HeqB.
eapply le_trans.
apply IHA.
rewrite add_comm.
apply le_add_r.
Qed.


Proposition Sum_tail: forall (A:list nat)(n a:nat),
  Sum (a::A) = n -> Sum A = n-a.
Proof.
intros.
simpl in H.
apply add_sub_eq_l in H.
rewrite H.
reflexivity.
Qed.

Proposition Sum_zeros: forall (k:nat),
  Sum (zeros k)=0.
Proof.
intros.
induction k.
simpl.
reflexivity.
simpl.
assumption.
Qed.
(** 2.-Propiedades de zeros*)

Proposition zeros_length: forall (m: nat), 
  length (zeros m) = m.
Proof.
induction m.
simpl.
reflexivity.
simpl.
rewrite IHm.
reflexivity.
Qed.

Proposition zeros_head:  forall (m : nat), 
 hd 0 (zeros m)= 0.
Proof.
intros.
destruct m.
simpl.
reflexivity.
simpl.
reflexivity.
Qed.

Proposition zeros_has_head1:  forall (m a: nat), 
 hd a (zeros (S m))= 0.
Proof.
intros.
simpl.
reflexivity.
Qed.

Proposition zeros_has_head2:  forall (m a: nat), 
  0<m -> hd a (zeros m)= 0.
Proof.
intros.
apply succ_pred_pos in H.
rewrite <-H.
simpl.
reflexivity.
Qed.

Proposition zeros_add_concat: forall (a b:nat),
   zeros (a+b)= zeros a ++ zeros b  .
Proof.
intros.
induction a.
simpl.
reflexivity.
rewrite add_succ_l.
simpl.
rewrite IHa.
reflexivity.
Qed.

(* zeros cumple con la prop que afirma que contiene 
puros ceros.*)
Proposition zeros_allzeros: forall (m : nat),
  allZeros (zeros m).
Proof.
intros.
induction m.
simpl.
constructor.
simpl.
constructor.
assumption.
Qed.

(*Cualquier lista que contenga puros ceros 
necesariamente es zeros*)
Proposition allzeros_is_zeros:forall (m : nat)(N:list nat),
  allZeros N -> N=zeros(length N).
Proof.
intros.
induction H.
simpl.
reflexivity.
simpl.
rewrite <-IHallZeros.
reflexivity.
Qed.
  
(* zeros es una lista descendiente*)
Proposition zeros_is_Des: forall (m : nat),    
  Des (zeros m).
Proof.
intros.
pose proof zeros_allzeros.
apply (allZeros_is_Des (zeros m)) in H.
assumption.
Qed.

(* zeros es una representación por separadores*)
Proposition zeros_is_Sep:  forall (n k: nat),
  Sep (zeros k) n k.
Proof.
intros.
split.
apply zeros_is_Des.
split.
rewrite zeros_head.
apply le_0_l.
apply zeros_length.
Qed.

(*zeros es el mínimo en el orden entre separadores*)
Proposition zeros_Sep_Min: forall (n k: nat)(A:list nat),
 Sep A n k -> leSep (zeros k) A n k.
Proof.
intros.
pose proof (zeros_allzeros k).
apply (leSep_Min k n A (zeros k)  )in H.
- assumption.
- assumption.
- apply zeros_length.
Qed.

Proposition zeros_is_nil_iff_nil:forall (k: nat),
  zeros k = [] <-> k=0.
Proof.
intros.
destruct k.
simpl.
split.
+ intro. reflexivity.
+ intro. reflexivity.
+ split. 
++ simpl.
  intro.
  inversion H.
++ intro.
   inversion H.
Qed.
(** 3.- Propiedades de enn*)

Proposition enn_length: forall (n m: nat), 
  length (enn n m) = m.
Proof.
induction m.
simpl.
reflexivity.
simpl.
rewrite IHm.
reflexivity.
Qed.

Proposition enn_head:  forall (n m a: nat), 
 hd n (enn n m)= n.
Proof.
intros.
destruct m.
simpl.
reflexivity.
simpl.
reflexivity.
Qed.

Proposition enn_has_head:  forall (n m a: nat), 
 hd a (enn n (S m))= n.
Proof.
intros.
simpl.
reflexivity.
Qed.

Proposition enn_has_head2:  forall (n m a: nat), 
  0<m -> hd a (enn n m)= n.
Proof.
intros.
apply succ_pred_pos in H.
rewrite <-H.
simpl.
reflexivity.
Qed.

Proposition enn_add_concat: forall (n a b:nat),
   enn n (a+b)= (enn n a) ++ (enn n b)  .
Proof.
intros.
induction a.
simpl.
reflexivity.
rewrite add_succ_l.
simpl.
rewrite IHa.
reflexivity.
Qed.

(*La lista de valores repetidos cumple con la
 propiedad que afirma que tiene todos los valores repetidos*)
Proposition enn_alln: forall (n len: nat),
  alln (enn n len) n.
Proof.
intros.
induction len.
simpl.
constructor.
simpl.
constructor.
assumption.
Qed.

(*Cualquier lista que cumpla con esta propiedad
necesariamiente es enn*)
Proposition alln_is_enn: forall (N :list nat)(n:nat),
  alln N n -> N = enn n (length N).
Proof.
intros.
induction H.
simpl.
reflexivity.
simpl.
rewrite <-IHalln.
reflexivity.
Qed.

(*La lista enn es descendiente*)
Proposition enn_is_Des: forall (n len : nat),    
  Des (enn n len).
Proof.
intros.
pose proof enn_alln.
apply (alln_is_Des (enn n len)) in H.
assumption.
Qed.

(*La lista enn es una representación por separadores*)
Proposition enn_is_Sep:  forall (n k: nat),
  Sep (enn n k) n k.
Proof.
intros.
split.
apply enn_is_Des.
split.
destruct k.
simpl.
apply le_0_l.
simpl.
constructor.
apply enn_length.
Qed.

(*la lista enn es el valor máximo entre sepraradores*)
Proposition enn_Sep_Max: forall (n k: nat)(A:list nat),
 Sep A n k -> leSep A (enn n k) n k.
Proof.
intros.
pose proof (enn_alln n k ).
apply (leSep_Max k n A (enn n k) )in H.
- assumption.
- assumption.
- apply enn_length.
Qed. 