(** Autor: Luis Felipe Benítez Lluis
	Número: 6
	*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_des . 
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 
From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_sep . 




(*Propiedades de orden para mostrar que los separadores
son un COTO reflexivo con mínimo y máximo*)

Proposition leSep_refl: forall (A:list nat)(n k:nat), 
  Sep A n k-> (leSep A A n k).
Proof.
  intros.
  split.
  assumption.
  split.
  assumption.
  apply leDes_refl.
  inversion H.
  assumption.
Qed.


Proposition leSep_antisym: forall (A B :list nat)(n k:nat),
  (leSep A B n k) -> (leSep B A n k)
  -> A = B.
Proof.
  intros.
  destruct H.
  destruct H1.
  destruct H0.
  destruct H3.
  apply leDes_antisym.
  + inversion H. assumption.
  + inversion H1. assumption.
  + assumption.
  + assumption.
Qed.


Proposition leSep_trans: forall (A B C:list nat)(n k:nat),
  (leSep A B n k) -> (leSep B C n k)-> (leSep A C n k).
Proof.
  intros.
  destruct H.
  destruct H1.
  destruct H0.
  destruct H3.
  split.
  assumption.
  split.
  assumption.
  eapply leDes_trans.
  5: { apply H4. }
  4: { assumption. }
  destruct H.
  assumption.
  destruct H1.
  assumption.
  destruct H3.
  assumption.
Qed. 


Proposition leSep_dich: forall (A B :list nat)(n k:nat),
  (Sep A n k)-> (Sep B n k)-> 
    ((leSep A B n k)\/(leSep B A n k)).
Proof.
  intros.
  pose proof leDes_dich as T.
  specialize T with (A:=A)(B:=B).
  pose proof H as H'.
  destruct H as [T1 T2].
  pose proof H0 as H''.
  destruct H0 as [R1 R2].
  apply T in T1.
  2: { assumption. }
  destruct T1.
  + left. split. assumption.
    split. assumption. assumption.
  + right. split. assumption.
    split. assumption. assumption.
Qed.
(* Probar que es buen orden *)

Proposition sep_head_mono: forall (A B :list nat)(a b n k :nat),
  leSep (a::A) (b::B) n k -> a<=b.
Proof.
  intros.
  inversion H.
  destruct H1.
  apply des_head_mono in H2.
  assumption.
Qed.


(* Veamos que para n fijo este orden tiene mínimo y mnáximo. *)
Proposition leSep_Min: forall  (k n:nat)(P A:list nat),
  allZeros A -> length A = k ->
  (Sep P n k) -> (leSep A P n k).
Proof.
  intro.
  induction k. 
  + intros n P A AZ LenA SP.  
    apply length_zero_iff_nil in LenA.
    inversion SP as [DP] .
    destruct H as [hdP LenP].
    split.
    - rewrite LenA.
      apply nil_is_Sep. 
    - split. 
      exact SP.
      rewrite LenA.
      apply length_zero_iff_nil in LenP.
      rewrite LenP.
      constructor.
  + intros n P A AZ LenA SP.
    inversion SP as [DP] .
    destruct H as [hdP LenP].
    apply (length_suc_head P k)in LenP as HdP.
    destruct HdP as [ a [T1 HdP] ].
    apply (length_suc_head A k)in LenA as HdA.
    destruct HdA as [ b [T2 HdA] ].
    subst.
    inversion AZ.
    split.
    apply (allZeros_is_Sep (b::T2) n (S k)) in AZ.
    { rewrite H. assumption. }
    { exact LenA. } 
    split.
    { exact SP. }
    pose proof (lt_trichotomy 0 a) as Tri.
    destruct Tri.
      { constructor. assumption. }
    destruct H2.
    2: { inversion H2. }
    apply leDesHH.
    subst.
    reflexivity.
    specialize (IHk n T1 T2 ).
    apply IHk in H0.
    2: { simpl in LenA.
         inversion LenA.
         reflexivity. }
    2: { apply remove_head_is_Sep2 in SP.
         assumption. }
    inversion H0.
    destruct H4.
    exact H5.
Qed.    

  
  
Proposition leSep_Max: forall (k n:nat)(P A:list nat),
  alln A n -> length A = k -> 
  (Sep P n k) -> (leSep P A n k).
Proof.
intro.
induction k.
+
intros n P A AN LenA SP. 
apply length_zero_iff_nil in LenA.
inversion SP as [DP] .
destruct H as [hdP LenP].
rewrite LenA.
apply length_zero_iff_nil in LenP.
rewrite LenP.
split.
{ apply nil_is_Sep. } 
split.
{ apply nil_is_Sep. }
{ constructor. }
+ intros n P A AN LenA SP.
  inversion SP as [DP] .
  destruct H as [hdP LenP].
  apply (length_suc_head P k)in LenP as HdP.
  destruct HdP as [ a [T1 HdP] ].
  apply (length_suc_head A k)in LenA as HdA.
  destruct HdA as [ b [T2 HdA] ].
  subst.
  inversion AN as [| T20 b0 ANT] .
  inversion SP.
  destruct H3.
  simpl in H3.
  split.
  { assumption. }
  split.
  { apply (alln_is_Sep (b :: T2)n (S k) )in AN.
    rewrite H1 in AN.
    assumption.
    assumption. }
  { apply lt_eq_cases in H3.
    destruct H3.
    * constructor.
      assumption.
    * apply leDesHH.
      assumption.
      specialize (IHk n T1 T2).
      apply IHk in ANT.
      inversion ANT.
      destruct H6.
      - assumption.
      - simpl in LenA.
        inversion LenA.
        reflexivity.
      - apply remove_head_is_Sep2 in SP.
        assumption. }
Qed.

