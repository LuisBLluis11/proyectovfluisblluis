(** Autor: Luis Felipe Benítez Lluis
	Número: 1
	*)

Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
(* From Coq Require Import Lia. *)
(* Parameter (undefnat : nat).
(* (eq_dec_Des: forall (x y:A),{x=y}+{x<>y}) *) *)
          
          
(*Relación inductiva para listas con valores descendientes*)
Inductive Des: list nat -> Prop:=
  | DesBase:  Des []
  | DesHtl: forall (t:list nat)(h:nat), 
        Des t-> hd 0 t<= h -> Des (h::t).
        
Example Ej1: Des ([3;2]).
Proof.
  constructor.
  + constructor.
    ++ constructor.
    ++ simpl. apply le_0_n.
  + simpl. constructor. constructor.
Qed.

(* Prop que afirma que una lista es de puros ceros*)
Inductive allZeros: list nat-> Prop:=
  | allZerosZ: allZeros []
  | allZerosHtl: forall(tl: list nat),
        allZeros tl -> allZeros (0::tl).
        
(* Prop que afirma que una lista consta de puros naturales n*)        
Inductive alln: list nat-> nat -> Prop:=
  | allnn: forall n:nat, alln [] n
  | allnHtl: forall(tl: list nat)(n:nat),
        (alln tl n)-> (alln (n::tl) n).

(* Relación de orden entre listas de valores descendientes.  
			El orden es lexicográfico en versión reflexiva.*)
Inductive leDes: list nat -> list nat ->  Prop:=
  |leDesNil: forall l: list nat,leDes [] l
  |leDesBase: forall l:list nat,leDes l l
  |leDesTT: forall (t1 t2:list nat)(h1 h2: nat),
         h1 < h2 ->  leDes (h1::t1) (h2::t2)
 |leDesHH: forall (t1 t2:list nat)(h1 h2: nat),
         h1 = h2 -> leDes t1 t2 ->
                    leDes (h1::t1) (h2::t2).

Notation "a <=Des b" := (leDes a b) (at level 70).