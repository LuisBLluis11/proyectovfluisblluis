(** Autor: Luis Felipe Benítez Lluis
	Número: 2
	*)

Require Import List.
Require Import PeanoNat.
Import ListNotations .
Import Nat.
From CompLL11 Require Import Defs_des .



 (** Lemas auxiliares *)
Lemma length_suc_iff_hdTl_G: forall(A:Type) (l:list A),
 ( exists (k:nat),length l =S k) <-> exists (a:A)(T:list A), l = a::T.
Proof.
intros.
split.
+ intros.
  destruct l.
  ++ simpl in H.
      destruct H.
     inversion H.
  ++ exists a , l.
     reflexivity. 
+ intros.
  destruct H as [a [T H]].
  rewrite H.
  simpl.
  exists (length T).
  reflexivity. 
Qed.
Lemma length_suc_head: forall (l:list nat)(k:nat),
 length l =S k -> exists (a:nat)(T:list nat), 
   l = a::T.
Proof.
  intros.
  destruct l.
  + simpl in H.
    inversion H.
  + simpl in H.
    exists n,l.
    reflexivity.
Qed. 


(* Propiedades de head y tail*)
Lemma add_head_is_Des: forall (l : list nat)(n:nat),
  Des l -> hd 0 l <= n -> Des (n::l).
Proof.
induction l.
intros.
constructor.
assumption.
assumption.
intros.
constructor.
assumption.
simpl.
simpl in H0.
assumption.
Qed.


Lemma Des_tail: forall l:list nat,
  Des l -> Des (tl l).
Proof.
  intros.
  induction l.
  simpl.
  constructor.
  simpl.
  inversion H.
  assumption.
Qed.

(*Propiedades de listas descendietnes*)

(*Lista descendiente*)
Lemma Des_desc: forall l: list nat,
  Des l -> hd 0 (tl l)<= hd 0 l.
Proof.
  intros.
  induction H.
  + simpl. apply le_0_n.
  + simpl. destruct t0. 
    ++ simpl.  apply le_0_n.
    ++ simpl in H0. apply H0.
Qed. 


(*Lista de ceros es descendiente*)
Lemma allZeros_is_Des: forall l : list nat,
  allZeros l -> Des l.
Proof.
  intros.
  induction H.
  + constructor.
  + constructor. 
    - assumption.
    - destruct tl.
      * simpl. constructor.
      * inversion H.
        simpl.
        constructor.
Qed.

(* Lista de repetición del natural n es descendiente*)
Lemma alln_is_Des: forall (l :list nat)(n:nat),
  (alln l n)-> Des l.
Proof.
  intros.
  induction H.
  + constructor.
  + constructor.
    assumption.
    inversion IHalln.
    simpl. apply le_0_n.
    simpl.
    rewrite <-H2 in H.
    inversion H.
    constructor.
Qed.

(* Propiedades para la concatenación *)
Lemma last_disyuctive: forall (l:list nat)(a  b:nat) ,
  last (a::l) b = last l b\/  last (a::l) b = a 
  \/ last (a::l) b = b.
Proof.
intro.
induction l.
+ simpl. right. left. reflexivity.
+ intros. specialize IHl with (a:= a)(b:=b).
  destruct IHl.
  remember (a::l) as l0.
  simpl. rewrite H. rewrite Heql0.
  left. reflexivity.
  remember (a::l) as l0.
  destruct H.
  simpl.
  rewrite H.
  rewrite Heql0.
  left.
  reflexivity.
  simpl.
  rewrite H.
  rewrite Heql0.
  right. right. 
  reflexivity.
Qed.

Lemma last_is_head: forall (l:list nat)(a b:nat),
  last (a:: l) a = a -> Des l -> last l a = a.
Proof.
intros.
simpl in H.
induction l.
simpl.
reflexivity.
simpl in H.
simpl.
rewrite H.
reflexivity.
Qed.


Lemma last_le_head_Des: forall l:list nat, 
  Des l->  last l 0 <= hd 0 l .
Proof.
induction l.
reflexivity.
simpl (hd 0 (a :: l)).
intros.
inversion H.
subst.
pose proof (last_disyuctive l a 0).
destruct H0.
rewrite H0.
apply IHl in H2.
eapply le_trans.
apply H2.
assumption.

destruct H0.
rewrite H0.
constructor.
rewrite H0.
apply le_0_l.
Qed.


(* Lemma concat_is_Des: forall l k : list nat,
  Des k -> Des l -> hd 0 k<= last l (hd 0 k)->
  Des (l ++ k).
Proof.
(* intros.
induction l.
simpl.
assumption.
rewrite <-app_comm_cons.
constructor.
  2: { inversion H.
  inversion H.
  subst.
+ apply IHl.
  assumption.
  pose proof (last_disyuctive l a (hd 0 k)).
  destruct H2.
  rewrite <-H2.
  assumption.
  destruct H2.
  rewrite H2 in H1.
  admit.
  apply Des_tail in H.
  simpl in H.
  assumption.
simpl in H1.
Compute hd 3 [1;2].
induction k.
simpl.
rewrite app_nil_r.
assumption. *)
Admitted. *)