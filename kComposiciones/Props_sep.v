(** Autor: Luis Felipe Benítez Lluis
	Número: 5
	*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_des . 
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 
From CompLL11 Require Import Defs_sep . 



Lemma allZeros_is_Sep: forall (l:list nat) (n k:nat),
  allZeros l -> length l = k -> Sep l n k.
Proof.
  intros.
  split.
  apply allZeros_is_Des.
  assumption.
  split.
  2: { assumption. }  
  induction H.
  simpl. apply le_0_n.
  simpl. apply le_0_n.
Qed.

Lemma alln_is_Sep:forall (l:list nat) (n k:nat),
  alln l n -> length l = k -> Sep l n k.
Proof.
  intros.
  split.
  apply alln_is_Des in H.
  assumption.
  split.
  2: {assumption. }
  induction H.
  simpl.
  apply le_0_n.
  constructor.
Qed.


Lemma nil_is_Sep :forall  (n:nat),
   Sep [] n 0.
Proof.
intros.
split.
constructor.
split.
simpl.
apply le_0_l.
reflexivity.
Qed.

Lemma single_is_Sep :forall (a n:nat),
   a<= n ->Sep [a] n 1.
Proof.
intros.
split.
constructor.
constructor.
simpl.
apply le_0_l.
split.
simpl.
assumption.
simpl.
reflexivity.
Qed.

Lemma remove_head_is_Sep:forall (P : list nat)(a n k:nat),
  Sep (a::P) n (S k) -> Sep P a k.
Proof.
intro.
induction P.
+ intros.
  inversion H. 
  destruct H1. 
  simpl in H2.
  inversion H2.
  apply nil_is_Sep.
+ intros. 
  inversion H. 
  destruct H1.
  remember (a::P) as P0.
  inversion H0.
  simpl in H1.
  simpl in H2.
  inversion H2.
  constructor.
  assumption.
  split.
  eapply le_trans.
  apply H6.
  constructor.
  reflexivity.
Qed.

Lemma remove_head_is_Sep2:forall (P : list nat)(a n k:nat),
  Sep (a::P) n (S k) -> Sep P n k.
Proof.
intro.
induction P.
+ intros.
  inversion H. 
  destruct H1. 
  simpl in H2.
  inversion H2.
  apply nil_is_Sep.
+ intros. 
  inversion H. 
  destruct H1.
  remember (a::P) as P0.
  inversion H0.
  simpl in H1.
  simpl in H2.
  inversion H2.
  constructor.
  assumption.
  split.
  eapply le_trans.
  apply H6.
  assumption.
  reflexivity.
Qed.
  
(*head and tail*)
Lemma add_head_is_Sep: forall (P : list nat)(n k:nat),
  Sep P n k -> Sep (n::P) n (S k).
Proof.
intros.
inversion H.
destruct H1.
split.
+ constructor.
  assumption.
  assumption.
+ split.
++ simpl.
   constructor.
++ simpl.
   rewrite H2.
   reflexivity.
Qed.

Lemma ext_Sep_base_is_Sep:forall (P : list nat)(n k:nat),
  Sep P n k -> Sep P (S n) k.
Proof.
intros.
inversion H.
inversion H1.
split.
assumption.
split.
eapply le_trans.
apply H2.
apply le_succ_diag_r.
assumption.
Qed.

Lemma ext_Sep_is_Sep:forall (P : list nat)(n m k:nat),
  Sep P n k ->  n <= m -> Sep P m k.
Proof.
intros.
inversion H.
inversion H2.
split.
assumption.
split.
eapply le_trans.
apply H3.
assumption.
assumption.
Qed.


Proposition upbound_Sep: forall (A B : list nat)(n k:nat),
  (Sep B n k)-> length A = k-> Des A ->
  (A <=Des B) -> Sep A n k. 
Proof.
  intros.
  split.
  assumption.
  split.
  2:{ assumption. }
  induction H2.
  + simpl. apply le_0_n.
  + destruct l.
    -- simpl. apply le_0_n.
    -- simpl. inversion H.
        simpl in *.
        destruct H3.
        apply H3.
  + simpl in *. 
    inversion H.
    simpl in *.
    destruct H4.
    eapply le_trans.
    2: { apply H4. }
    apply lt_le_incl.
    assumption.
  + inversion H.
    simpl in *.
    destruct H5.
    rewrite H2.
    assumption.
Qed.