(** Autor: Luis Felipe Benítez Lluis
	Número: 4
	*)
Require Import List.
From CompLL11 Require Import Defs_des . 



(* Proposición que dice si la lista describe una 
k+1 composición de n en su descripción por separadores
(k es la cantidad de separdores)*)
Definition Sep (l:list nat)(n:nat)(k:nat):=
  Des l /\ hd (0) l <= n /\ length l = k.


(* Relación de orden reflexivo para Separadores*)

Definition leSep (A B :list nat)(n k:nat):= 
   (Sep A n k)/\ (Sep B n k)/\ (A <=Des B ).
     
Notation "a <=Sep b / n ; k" := (leSep a b n k) (at level 70, b at next level).



(*Version obsoleta*)
(* Definition hasNext (A: list nat)(n k:nat):=
    S (hd 0 A) <= n /\ Sep A /\ length A = k.
  
Proposition hasNext_is_Sep: forall (P: list nat) (n k:nat), 
  hasNext P n k -> Sep P n k.
Proof.
  intros.
  destruct H.
  destruct H0.
  split.
  assumption.
  split.
  2:{ assumption. }
  pose proof le_succ_diag_r as T.
  specialize T with (n:= (hd 0 P)).
  eapply le_trans.
  apply T.
  assumption.
Qed. *)