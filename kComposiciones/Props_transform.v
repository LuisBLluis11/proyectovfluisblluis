(** Autor: Luis Felipe Benítez Lluis
	Número: 13
	*)
Require Import List.
Require Import PeanoNat.
Require Import Bool .
Import ListNotations.
Import Nat.
From Coq Require Import Lia.

From CompLL11 Require Import Defs_des .
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 

From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_sep .
From CompLL11 Require Import Props_Ordsep . 

From CompLL11 Require Import Defs_aux .
From CompLL11 Require Import Props_aux .

From CompLL11 Require Import Defs_next .
From CompLL11 Require Import Props_next .

From CompLL11 Require Import Defs_com .
From CompLL11 Require Import Props_com .

(** Propiedades Auxiliares de listas*)
Lemma Concat_zero_has_head:forall (A:list nat),
  exists (a:nat)(t:list nat),
    a::t =A++[0].
Proof.
intros.
induction A.
+ simpl.
  exists 0 , [].
  reflexivity.
+ simpl.
  destruct IHA.
  destruct H.
  rewrite <-H.
  exists a, (x::x0).
  reflexivity.
Qed.


Lemma Concat_zero_length:forall (A:list nat),
 length(A++[0])= S (length A).
Proof.
induction A.
simpl.
reflexivity.
simpl.
rewrite ( app_length A [0]).
simpl.
lia.
Qed.

(** Propiedades de ToCom*)
Proposition ToComAux_length:forall (A:list nat)(n:nat),
  pred (length A) = length (ToComAux A).
Proof.
intros.
induction A.
simpl.
reflexivity.
simpl.
destruct A.
reflexivity.
simpl.
simpl (pred (length (n0 :: A))) in IHA.
rewrite IHA.
simpl.
reflexivity.
Qed.




Proposition ToCom_length:forall (A:list nat)(n:nat),
  S(length A )= length (ToCom A n).
Proof.
intros.
induction A.
simpl.
reflexivity.
simpl.
pose proof (Concat_zero_has_head A) as W.
destruct W.
destruct H.
rewrite <-H.
remember (ToComAux (x :: x0)) as B.
simpl.
rewrite HeqB.
rewrite <-ToComAux_length.
rewrite H.
rewrite Concat_zero_length.
simpl.
reflexivity.
apply n.
Qed.
Proposition ToCom_nil: forall (n: nat),
  ToCom [] n = [n].
Proof.
intros.
simpl.
unfold ToCom.
simpl.
rewrite sub_0_r.
reflexivity.
Qed.

Proposition ToCom_intro: forall (A:list nat)(a n:nat),
  ToCom (a::A) n= (n-a)::(ToCom A a).
Proof.
intro.
unfold ToCom.
simpl.
reflexivity.
Qed.


Proposition ToCom_Sum:forall (A:list nat)(n k:nat),
  Sep A n k -> Sum (ToCom A n)=n.
Proof.
intro.
induction A.
simpl.
lia.
intros.
rewrite ToCom_intro .
simpl.
destruct k.
+
  inversion H.
  destruct H1.
  simpl in H2.
  inversion H2.
+ inversion H.
  destruct H1.
  apply remove_head_is_Sep in H.
  apply IHA in H.
  rewrite H.
  apply sub_add.
  simpl in H1.
  assumption.
Qed.

Proposition ToCom_is_Com:forall (A:list nat)(n k:nat),
  Sep A n k -> Com (ToCom A n) n k.
Proof.
intros.
split.
- apply ToCom_Sum in H.
  assumption.
- rewrite <-ToCom_length.
  inversion H.
  destruct H1.
  rewrite H2.
  reflexivity.
Qed.

(** Propiedades de Sep*)
Proposition ToSep_single:forall(n:nat),
  ToSep [n] = [].
Proof.
intros.
simpl.
reflexivity.
Qed.

(*Recordar que Com A n k afirma que A es una 
k+1-composición de n.*)
Proposition ToSep_length:forall(A:list nat)(a:nat),
  length (ToSep (a :: A)) = length A.
Proof.
intro.
induction A.
+ intros. simpl. reflexivity.
+ intros.
  simpl (length (a :: A)).
  specialize (IHA a). 
  rewrite <-IHA.
  simpl.
  reflexivity.
Qed.

Proposition ToSep_intro: forall (a b:nat)(A:list nat),
  ToSep(a::b::A)= Sum(b::A)::ToSep(b::A).
Proof.
intros.
simpl.
reflexivity.
Qed.


Proposition ToSep_head_bound:forall(A:list nat),
  hd 0 (ToSep A) <= Sum A.
Proof.
intros.
destruct A.
simpl.
constructor.
simpl.
destruct A.
simpl.
rewrite add_comm.
apply le_add_r.
remember (ToSep (n :: A)) as B.
simpl.
rewrite (add_comm n (n0 + Sum A)).
apply (le_add_r (n0 + Sum A) n).
Qed.



Proposition ToSep_is_des:forall(A:list nat),
  Des (ToSep A).
Proof.
intros.
induction A.
simpl.
constructor.

destruct A.
simpl.
constructor.
rewrite ToSep_intro.
constructor.
assumption.
apply ToSep_head_bound.
Qed.

  
Proposition ToSep_is_sep:forall(A:list nat),
  Sep (ToSep A) (Sum A) (pred (length A)).
Proof.
intros.
split.
apply ToSep_is_des.
split.
apply ToSep_head_bound.
destruct A.
simpl.
reflexivity.
simpl (pred (length (n :: A))).
apply ToSep_length.
Qed.


Proposition ToSep_is_sep2:forall(A:list nat)(n k:nat),
Com A n k -> Sep (ToSep A) n k.
Proof.
intros.
inversion H.
split.
apply ToSep_is_des.
split.
rewrite <-H0.
apply ToSep_head_bound.
pose proof (pred_succ k) as W.
rewrite <-W.
rewrite <-H1.
destruct A.
simpl in H1.
inversion H1.
simpl (pred (length (n0 :: A))).
apply ToSep_length.
Qed.

(** Propiedades de inversión entre transformaciones*)
Theorem ToCom_ToSep_inv:forall(A:list nat)(n k:nat),
  Com A n k -> ToCom (ToSep A) n= A.
Proof.
intro.
induction A.
+ intros.
  contradict H.
  apply nil_isnot_com.
+ intros.
  destruct A.
  { simpl.
    unfold ToCom.
    simpl.
    rewrite sub_0_r.
    inversion H.
    simpl in H0.
    rewrite <-plus_n_O in H0.
    subst.
    reflexivity. }
  rewrite ToSep_intro.
  rewrite ToCom_intro.
  inversion H.
  remember (n0::A) as B.
  simpl in H1.
  inversion H1.
  specialize (IHA (n-a) (length A)).
  rewrite HeqB in H3.
  simpl in H3.
  rewrite <-H3 in H.
  apply remove_head_is_com in H.
  apply IHA in H.
  simpl in H0.
  rewrite <-H0.
  apply add_sub_eq_l in H0.
  rewrite add_sub.
  rewrite <-H0.  
  rewrite H.
  reflexivity.
Qed.

Theorem ToCom_ToSep_inv2:forall(A:list nat),
  A<> [] ->ToCom (ToSep A) (Sum A)= A.
Proof.
intros.
pose proof (allCom A).
apply (ToCom_ToSep_inv  A (Sum A) (pred (length A))).
apply H0.
assumption.
Qed.

Theorem ToSep_ToCom_inv: forall (A:list nat)(n k:nat),
 Sep A n k -> ToSep ( ToCom A n)=A.
Proof.
intro.
induction A.
{
simpl.
reflexivity. }
intros.
destruct A.
- simpl. 
  rewrite sub_0_r.
  rewrite <-plus_n_O.
  reflexivity.
- assert (Sep (n0 :: A) a (S (length A))) as W.
  {
  inversion H.
  destruct H1.
  simpl in H2.
  rewrite <-H2 in H.
  apply remove_head_is_Sep in H.
  exact H. }
  
  rewrite ToCom_intro.
  rewrite ToCom_intro.
  rewrite ToSep_intro.
  rewrite <-ToCom_intro.
  rewrite IHA with (k:=(S( length A))).
  rewrite ToCom_Sum with (k:= S(length A)).
  reflexivity.
  assumption.
  assumption.
Qed.
  
Proposition ToCom_inj: forall (A B:list nat)(n k:nat),
  Sep A n k -> Sep B n k-> ToCom A n = ToCom B n->
  A=B.
Proof.
intros.
apply ToSep_ToCom_inv in H.
apply ToSep_ToCom_inv in H0.
rewrite <-H.
rewrite <- H0.
rewrite H1.
reflexivity.
Qed.

Proposition ToVal_inj: forall (A B:list nat)(n k :nat),
 Com A n k-> Com B n k->  ToSep A = ToSep B -> A = B.
Proof.
intros.
apply ToCom_ToSep_inv in H.
apply ToCom_ToSep_inv in H0.
rewrite <-H.
rewrite <-H0.
rewrite H1.
reflexivity.
Qed.

(** Tranformaciones entre elementos mínimos y máximos en
    en composiciones y sus representaciones por serparadores*)

Proposition ToSep_exxl_is_zeros:
  forall (k n:nat), ToSep (exxl n k) = zeros k.
Proof.
intro.
induction k.
intros.
simpl.
reflexivity.
intros.
unfold exxl in *.
simpl (zeros (S k)).
rewrite ToSep_intro.
rewrite (IHk 0).
simpl.
rewrite Sum_zeros.
reflexivity.
Qed.

Proposition ToSep_exxr_is_enn:
  forall (k n:nat), ToSep (exxr n k) = enn n k.
Proof.
intro.
induction k.
simpl.
reflexivity.
intros.
unfold exxr in *.
destruct k.
simpl.
rewrite <-(plus_n_O n).
reflexivity.
simpl (zeros (S (S k))).
rewrite <-app_comm_cons.
rewrite <-app_comm_cons.
rewrite ToSep_intro.
specialize (IHk n).
simpl (zeros (S k))in IHk.
rewrite <-app_comm_cons in IHk.
rewrite IHk.
simpl.
rewrite Sum_concat.
rewrite Sum_zeros.
simpl.
rewrite <- (plus_n_O n).
reflexivity.
Qed.
