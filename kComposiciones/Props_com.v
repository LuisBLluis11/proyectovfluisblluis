(** Autor: Luis Felipe Benítez Lluis
	Número: 12
	*)
Require Import List.
Require Import PeanoNat.
Require Import Bool .
Import ListNotations.
Import Nat.
From Coq Require Import Lia.

From CompLL11 Require Import Defs_des .
From CompLL11 Require Import Props_des . 
From CompLL11 Require Import Props_Orddes . 

From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_sep .
From CompLL11 Require Import Props_Ordsep . 

From CompLL11 Require Import Defs_aux .
From CompLL11 Require Import Props_aux .

From CompLL11 Require Import Defs_next .
From CompLL11 Require Import Props_next .

From CompLL11 Require Import Defs_com .



(** Propiedades básicas*)
Proposition nil_isnot_com: forall (n k : nat),
  ~Com [] n k.
Proof.
intros.
intro.
destruct H.
inversion H0.
Qed.
Proposition single_is_com: forall (n:nat),
  Com [n] n 0.
Proof.
intros.
split.
simpl.
rewrite plus_n_O.
reflexivity.
simpl.
reflexivity.
Qed.


Proposition allCom: forall (A: list nat),
  A<> []-> Com A (Sum A) (pred (length A)).
Proof.
intros.
destruct A. 
contradict H.
reflexivity.
split.
reflexivity.
rewrite succ_pred.
reflexivity.
simpl.
intro.
inversion H0.
Qed.

(** Propiedades de adición y remosión *)
Proposition remove_head_is_com: forall (A:list nat)(a n k :nat),
  Com (a::A) n (S k)-> Com A (n-a) k.
Proof.
intros.
destruct H.
apply Sum_tail in H.
simpl in H0.
inversion H0.
split.
assumption.
assumption.
Qed.

Proposition ext_head_is_com:forall (A:list nat)(a n k :nat),
  Com A n k-> Com (a::A) (n+a) (S k).
Proof.
intros.
inversion H.
split.
- simpl.
  rewrite H0.
  lia.
- simpl.
  auto.
Qed.

Proposition head_bound_com:forall (A:list nat)(n k :nat),
  Com A n k-> hd 0 A <= n.
Proof.
induction A.
intros.
contradict H.
apply nil_isnot_com.
intros.
inversion H.
rewrite <-H0.
apply (Sum_gt_head (a::A)).
Qed.

Proposition last_bound_com:forall (A:list nat)(n k :nat),
  Com A n k-> last A 0 <= n.
Proof.
intros.
inversion H.
rewrite <-H0.
apply Sum_gt_last.
Qed.

(** Propiedades sobre funciones exxl y exxr*)

Proposition Sum_exxl: forall (k n:nat),
  Sum (exxl n k)= n.
Proof.
intro.
induction k.
intros.
simpl.
rewrite plus_n_O.
reflexivity.
intros.
unfold exxl.
simpl.
rewrite Sum_zeros.
rewrite plus_n_O.
reflexivity.
Qed.
Proposition Sum_exxr: forall (k n:nat),
  Sum (exxr n k)= n.
Proof.
intro.
induction k.
intros.
simpl.
rewrite plus_n_O.
reflexivity.
intros.
unfold exxr in *.
simpl.
rewrite Sum_concat.
rewrite Sum_zeros.
simpl.
rewrite plus_n_O.
reflexivity.
Qed.

Proposition exxl_length: forall (k n:nat),
  length (exxl n k)= S k.
Proof.
intro.
induction k.
simpl.
reflexivity.
intros.
unfold exxl in *.
simpl.
rewrite zeros_length.
reflexivity.
Qed.

Proposition exxr_length: forall (k n:nat),
  length (exxr n k)= S k.
Proof.
intro.
induction k.
simpl.
reflexivity.
intros.
unfold exxr in *.
simpl.
rewrite (IHk n).
reflexivity.
Qed.

Proposition exxl_is_com:forall (k n:nat),
  Com (exxl n k) n k.
Proof.
intro.
induction k.
intros.
unfold exxl.
simpl.
apply single_is_com.
intros.
unfold exxl.
split.
simpl.
rewrite Sum_zeros.
rewrite plus_n_O.
reflexivity.
simpl.
rewrite zeros_length.
reflexivity.
Qed.


  
Proposition exxr_is_com:forall (k n:nat),
  Com (exxr n k) n k.
Proof.
intro. 
induction k.
unfold exxr.
simpl.
apply single_is_com.
intros.
unfold exxr in *.
simpl (zeros (S k) ).
rewrite <-app_comm_cons.
rewrite (plus_n_O n).
apply ext_head_is_com.
rewrite <-(plus_n_O n).
apply IHk.
Qed.

  
  
  