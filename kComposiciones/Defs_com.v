(** Autor: Luis Felipe Benítez Lluis
	Número: 11
	*)
Require Import List.
Require Import PeanoNat.
Require Import Bool .
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_sep . 
From CompLL11 Require Import Props_Ordsep .  

From CompLL11 Require Import Defs_aux .
From CompLL11 Require Import Props_aux .

From CompLL11 Require Import Defs_next .

(** 1.- Composiciones de n en k+1 partes*)
(* Definición de una k+1-composición de n. 
    Nótese que el parámetro es k ya que 
    no se permiten composiciones vacías. 
    Es importante resaltar que las representaciones
    por seperadores son de tamaño k*)
Definition Com (l:list nat)(n:nat)(k:nat):=
  Sum l = n /\ length l = S k.


(** 2.- Función que transforma composiciones 
        en si representación por separadores*)
Fixpoint ToSep (l:list nat):list nat:=
  match l with 
    | [] => [] (*si Com l, entonces caso imposible*)
    | _::t => match t with 
              | [] => []
              | _::_ =>Sum t::(ToSep t)
              end
    end.

(** 3.- Función que transforma representación por
     separadores en composiciones*)

Fixpoint ToComAux (l:list nat):list nat:=
  match l with 
    |[] => [] 
    | a::t => match t with 
              |[]=> []
              |b::_ => (a-b)::(ToComAux (t))
              end
  end.
  
Definition ToCom (l :list nat)(n:nat):list nat:=
  ToComAux (n::(l++[0])).
  


(** 4.- Orden en composiciones que se define 
        en términos del orden de separadores*)
        
Definition  leCom (A B :list nat)(n k:nat):=
  (Com A n k ) /\ (Com B n k) /\ (leSep (ToSep A) (ToSep B) n k).
Notation "a <=Com b / n ; k" := (leCom a b n k) (at level 70, b at next level).


(** 5.- Función sucesor de composiciones en términos
        de la función sucesor de separadores*)
Definition NextC (l:list nat): list nat:=
  (ToCom (Next (ToSep l)) (Sum l)).


(* Ejemplos de cómputos de la transformación 
de todas las de todas las 4-composiciones de 3
a su representaciones por separadores *)
Compute ToCom [0;0;0] 3.
Compute ToCom [1;0;0] 3.
Compute ToCom [1;1;0] 3.
Compute ToCom [1;1;1] 3.
Compute ToCom [2;0;0] 3.
Compute ToCom [2;1;0] 3.
Compute ToCom [2;1;1] 3.
Compute ToCom [2;2;0] 3.
Compute ToCom [2;2;1] 3.
Compute ToCom [2;2;2] 3.
Compute ToCom [3;0;0] 3.
Compute ToCom [3;1;0] 3.
Compute ToCom [3;1;1] 3.
Compute ToCom [3;2;0] 3.
Compute ToCom [3;2;1] 3.
Compute ToCom [3;2;2] 3.
Compute ToCom [3;3;0] 3.
Compute ToCom [3;3;1] 3.
Compute ToCom [3;3;2] 3.
Compute ToCom [3;3;3] 3.


(* mas ejemplos*)
Compute ToCom [1;2] 2.
Compute ToCom [6;4;3;2] 6.
Compute Sum [0; 2; 1; 1; 2].
Compute ToCom [] 5.

(*Verifiación que todas las transformaciones son 
en efecto composiciones de 3*)
Compute  Sum (ToCom [0;0;0]  3).
Compute  Sum (ToCom [1;0;0]  3).
Compute  Sum (ToCom [1;1;0]  3).
Compute  Sum (ToCom [1;1;1]  3).
Compute  Sum (ToCom [2;0;0]  3).
Compute  Sum (ToCom [2;1;0]  3).
Compute  Sum (ToCom [2;1;1]  3).
Compute  Sum (ToCom [2;2;0]  3).
Compute  Sum (ToCom [2;2;1]  3).
Compute  Sum (ToCom [2;2;2]  3).
Compute  Sum (ToCom [3;0;0]  3).
Compute  Sum (ToCom [3;1;0]  3).
Compute  Sum (ToCom [3;1;1]  3).
Compute  Sum (ToCom [3;2;0]  3).
Compute  Sum (ToCom [3;2;1]  3).
Compute  Sum (ToCom [3;2;2]  3).
Compute  Sum (ToCom [3;3;0]  3).
Compute  Sum (ToCom [3;3;1]  3).
Compute  Sum (ToCom [3;3;2]  3).
Compute  Sum (ToCom [3;3;3]  3).

(* verificación de funciones inversas*)
Compute ToSep (ToCom [0;0;0]3 ).
Compute ToSep (ToCom [1;0;0]3 ).
Compute ToSep (ToCom [1;1;0]3 ).
Compute ToSep (ToCom [1;1;1]3 ).
Compute ToSep (ToCom [2;0;0]3 ).
Compute ToSep (ToCom [2;1;0]3 ).
Compute ToSep (ToCom [2;1;1]3 ).
Compute ToSep (ToCom [2;2;0]3 ).
Compute ToSep (ToCom [2;2;1]3 ).
Compute ToSep (ToCom [2;2;2]3 ).
Compute ToSep (ToCom [3;0;0]3 ).
Compute ToSep (ToCom [3;1;0]3 ).
Compute ToSep (ToCom [3;1;1]3 ).
Compute ToSep (ToCom [3;2;0]3 ).
Compute ToSep (ToCom [3;2;1]3 ).
Compute ToSep (ToCom [3;2;2]3 ).
Compute ToSep (ToCom [3;3;0]3 ).
Compute ToSep (ToCom [3;3;1]3 ).
Compute ToSep (ToCom [3;3;2]3 ).
Compute ToSep (ToCom [3;3;3]3 ).

(* verificación de funciones inversas*)
Compute ToCom ( ToSep [3; 0; 0; 0] ) 3 .
Compute ToCom ( ToSep [2; 1; 0; 0] ) 3 .
Compute ToCom ( ToSep [2; 0; 1; 0] ) 3 .
Compute ToCom ( ToSep [2; 0; 0; 1] ) 3 .
Compute ToCom ( ToSep [1; 2; 0; 0] ) 3 .
Compute ToCom ( ToSep [1; 1; 1; 0] ) 3 .
Compute ToCom ( ToSep [1; 1; 0; 1] ) 3 .
Compute ToCom ( ToSep [1; 0; 2; 0] ) 3 .
Compute ToCom ( ToSep [1; 0; 1; 1] ) 3 .
Compute ToCom ( ToSep [1; 0; 0; 2] ) 3 .
Compute ToCom ( ToSep [0; 3; 0; 0] ) 3 .
Compute ToCom ( ToSep [0; 2; 1; 0] ) 3 .
Compute ToCom ( ToSep [0; 2; 0; 1] ) 3 .
Compute ToCom ( ToSep [0; 1; 2; 0] ) 3 .
Compute ToCom ( ToSep [0; 1; 1; 1] ) 3 .
Compute ToCom ( ToSep [0; 1; 0; 2] ) 3 .
Compute ToCom ( ToSep [0; 0; 3; 0] ) 3 .
Compute ToCom ( ToSep [0; 0; 2; 1] ) 3 .
Compute ToCom ( ToSep [0; 0; 1; 2] ) 3 .
Compute ToCom ( ToSep [0; 0; 0; 3] ) 3 .

(** 6.- Funciones extremo que tienen el valor
      provisto en un extremo y seguidos de ceros
      da acuerdo a la cantidad povista 
      (esta cantida refleja la cantidad de zeros 
      y no la longitud de la lista)*)
      
(*Extremo izquierdo*)      
Definition exxl (n k :nat):list nat:=
  n:: (zeros k).
  
(*Extremo derecho*)      
Definition exxr (n k :nat):list nat:=
  (zeros k)++[n].
