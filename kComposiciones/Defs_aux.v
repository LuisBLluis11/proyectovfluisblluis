(** Autor: Luis Felipe Benítez Lluis
	Número: 7
	*)
Require Import List.
Require Import PeanoNat.

Import ListNotations.
Import Nat.


(** Suma*)
(* Función que suma todos los valores de una lista.         *)
Fixpoint Sum (l:list nat):nat:=
  match l with 
    | [] => 0
    | h::t => h + Sum t
    end.

(** Lista de puros ceros*)

(* Función que devuelve una lista con puros
valores 0 de longitud provista*)
Fixpoint zeros (m:nat): list nat:=
  match m with 
    | 0 => []
    | S a => 0::(zeros a)
    end.
    
(** Lista con repetición de valores n*)  

(* Función que devuelve una lista con puros
valores n provista y de longitud provista*)
Fixpoint enn (n m:nat): list nat :=
  match m with 
    | 0 => []
    | S a => n ::(enn n a)
    end.