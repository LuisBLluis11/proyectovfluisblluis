(** Autor: Luis Felipe Benítez Lluis
	Número: 9
	*)
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From CompLL11 Require Import Defs_aux . 


(** 1.-  hasNext para separadores*)

(* Función que dada una lista devuelve un booleano
el cual es true cuando en el orden de separadores
la lista no es máxima*)
(** NOTA  Esta función tiene el comportamiento
          buscado cuando la lista es descendiente. 
          Si la lista no es descendiente puede que la
          función se comporte de una manera no tan intuitiva*)
Fixpoint hasNext (l:list nat)(n:nat):bool:=
  match l with 
    | [ ] => false
    | h::t=>  (h <? n) || ((h=?n) && (hasNext t h))
    end. 

(* ejemplos a los que no nos enfrentaremos*)
Compute hasNext [4;4;4;4;4] 4.
Compute hasNext [4;3] 4.
Compute hasNext [5;4] 3.
Compute hasNext [3;4] 3.
Compute hasNext [2;4] 3.
Compute hasNext [3;4] 4.
Compute hasNext [3;4] 5.
Compute hasNext [ ] 0.
Compute hasNext [ ] 2.
Compute hasNext [5;0;0;0] 4.

(* Para las representaciones por separadores 
   de todas las composiciones de 3 en 4 partes 
   hasNext funciona Bien*)
Compute hasNext [0;0;0] 3.
Compute hasNext [1;0;0] 3.
Compute hasNext [1;1;0] 3.
Compute hasNext [1;1;1] 3.
Compute hasNext [2;0;0] 3.
Compute hasNext [2;1;0] 3.
Compute hasNext [2;1;1] 3.
Compute hasNext [2;2;0] 3.
Compute hasNext [2;2;1] 3.
Compute hasNext [2;2;2] 3.
Compute hasNext [3;0;0] 3.
Compute hasNext [3;1;0] 3.
Compute hasNext [3;1;1] 3.
Compute hasNext [3;2;0] 3.
Compute hasNext [3;2;1] 3.
Compute hasNext [3;2;2] 3.
Compute hasNext [3;3;0] 3.
Compute hasNext [3;3;1] 3.
Compute hasNext [3;3;2] 3.
Compute hasNext [3;3;3] 3.

(** 2.- Next para separadores*)
(* Función que dada una lista, devuelve la siguiente
en el orden de separadores. Esta función está definida 
para todas las listas, y no es inyectiva en general,
pero si se restringe a las listas con separadores si 
es inyectiva. Esta función sí es monótona en el orden 
lexicográfico de listas. *)
Fixpoint Next (l:list nat): list nat:=
  match l with 
    | [] => []
    | h::t => match (hasNext t h) with 
              | true => h::(Next t)
              | false => (S h):: (zeros (length t))
              end
    end.
(*Ejemplos sencillos*)    
Compute Next [4;3] .  
Compute Next [5;4;4;4;1 ] .
Compute Next [6;0;0;0] .
Compute Next [6;1;0;0] .
(*Ejemplos de todas las descomposiciones de 3 en 4 partes*)
Compute Next [0;0;0] .
Compute Next [1;0;0] .
Compute Next [1;1;0] .
Compute Next [1;1;1] .
Compute Next [2;0;0] .
Compute Next [2;1;0] .
Compute Next [2;1;1] .
Compute Next [2;2;0] .
Compute Next [2;2;1] .
Compute Next [2;2;2] .
Compute Next [3;0;0] .
Compute Next [3;1;0] .
Compute Next [3;1;1] .
Compute Next [3;2;0] .
Compute Next [3;2;1] .
Compute Next [3;2;2] .
Compute Next [3;3;0] .
Compute Next [3;3;1] .
Compute Next [3;3;2] .
Compute Next [3;3;3] .
(*Ejemplos Dejenerados*)
Compute Next [3; 5].
Compute Next [3; 6].
Compute Next [3; 7].
