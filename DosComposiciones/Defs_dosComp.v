(** Autor: Luis B. Lluis LL11
  script de definiciones de una 2-composición*)


(* ComV2 es una proposición que nos dice si una pareja ordenanda
  P es una 2-composición de un natural n bajo la deficinón usual.
  Llamaremos a esta representación como representación por valores*)
Definition ComV2 (P: nat*nat) (n:nat) := 
            fst P + snd P =n. 

(* Las Definiciones toSep2c y toVal2C transforman una representación en la otra.
   Mostramos que una es inversa de la otra y por tanto una biyección*)
(*  Cuando se generalice se usará un fixpoint*)
Definition toSep2C (P : nat*nat): nat:= fst P.

Definition toVal2C (i n: nat): nat*nat := 
  (i,n-i).
(*Esta proposición afirma que el separador i representa una 2-composición de 
  n bajo la represntación por barras y estrellas. En esta representación
  los separadores sólo pueden moverse entre 0 y n.*)

Definition Com2 i n := i <= n. 
(* i es una estructura de datos que representa un separador,
no todos los separadores se ajustan a representar ser una composición.
La propiedad definida asegura que si representen una composición. 
Esto es análogo a que no todos los árboles binarios son árboles de braun,
pero si se les pide una propiedad de balanceo ya se vuelven de braun*)
  
(** Definición de orden en Composiciones*)

(*Esta definición de orden precisa tener dos sepradores que
representen una composición.*)
Definition leC2 i j n := (i <= j) /\  (Com2 i n) /\ (Com2 j n).

(* Definición de versión estricta del mismo orden*)
Definition leSC2 i j n := leC2 (S i) j n .
  
(* Definamos la proposición hasNext *)
Definition hasNext2C i n := S i <= n.

(* Sucesor para composiciones*)
Definition next2C i := S i.
  
  