(** Autor: Luis B. Lluis LL11
  script que desarrolla las propiedades básicas de una 
  2-composición*)

  
Require Import List.
Require Import PeanoNat.
Import ListNotations.
Import Nat.
From Coq Require Import Lia.
From CompLL11 Require Import Defs_dosComp .


(** 1. Repreentaciones de 2composiciones de un número n*)

(* Por valores *)
Print ComV2.
(* Por Separadores *)
Print Com2.

(* Ejemplos canónicos de 2-composiciones Valores *)
Example zero_n_ComV2: forall n:nat, 
  ComV2 (0,n) n.
Proof. intros;reflexivity.
Qed.

Example n_zero_ComV2: forall n:nat, 
  ComV2 (n,0) n.
Proof. 
  intros. 
  unfold ComV2. 
  simpl.
  apply add_0_r.
Qed.

(* Ejemplos canónicos de 2-composiciones Separadores *)

Example zero_Com2: forall n:nat, 
  Com2 0 n.
Proof. apply le_0_n.
Qed.
Example n_Com2: forall n:nat, 
  Com2 n n.
Proof. constructor.
Qed.



(** 2. Equivalencia de repreentaciones de 2composiciones *)

(* Funciones de traducción de una representación en otra*)
Print toSep2C.
Print toVal2C.

(* Propiedades de equivalencia *)
Proposition equiv_Val_Sep_2C: forall n i,
  toSep2C (toVal2C i n) = i.
Proof.
  intros.
  simpl.
  reflexivity.
Qed.

Proposition equiv_Sep_Val_2C:  forall (n:nat)(P:nat*nat),
  (ComV2 P n) ->(toVal2C(toSep2C P) n)= P.
Proof.
  intros.
  unfold toSep2C.
  destruct P as [x y].
  simpl.
  unfold ComV2 in H.
  unfold fst.
  simpl in H.
  unfold toVal2C.
  assert (y = n-x).
  + lia.
  + rewrite H0.
    reflexivity.
Qed.

(* Las funciones toVal y toSep son morfismos que preservan 
  la noción de ser una composición en sus distintas representaciones*)
Proposition morf_Val_Sep2C: forall (P:nat*nat)(n:nat),
    ComV2 P n -> Com2 (toSep2C P) n.
Proof.
  intros.
  destruct P as [x y].
  simpl.
  unfold ComV2 in H.
  simpl in H.
  unfold Com2.
  rewrite <- H.
  apply Plus.le_plus_l.
Qed.
  
Proposition morf_Sep_Val2C: forall (i n:nat),
    Com2 i n -> ComV2 (toVal2C i n) n.
Proof.
  intros.
  unfold toVal2C.
  unfold ComV2.
  simpl.
  unfold Com2 in H.
  lia.
Qed.

(** 3. Orden en 2composiciones *)
(* Procederemos a definir un BuenOrden entre las 
  2-composiciones de un número n. Este orden tendrá mínimo
  y máximo. Al ser un BO, se podrá definir un sucesor
  al que denominaremos next. Esto último solo se puede hacer
  para todos los elementos que no sean el máximo, por lo que definiremos
  hasNext como una proposición que nos dirá si tiene siguiente elementos. 
  
  La representación de las composiciones mediante separadores
  hace más sencilla su comprensión y ordenamiento, por lo que tomaremos
  esta representación como a predeterminada. Cualquier traducción a 
  su representación usual por valores se puede hacer con toVal2C
  Demostremos que este orden en un COPO reflexivo,
  esto es, reflexivo, antisimétrico, y transitivo
*)

Proposition leC2_refl: forall i n:nat,
   Com2 i n ->leC2 i i n .
Proof.
  intros.
  unfold leC2.
  split.
  unfold Com2 in H.
  constructor.
  split. assumption.
  assumption.
Qed.

Proposition leC2_trans: forall i j k n:nat,  
  (leC2 i j n) -> (leC2 j k n)-> (leC2 i k n).
Proof.
  intros.
  destruct H. destruct H1. 
  destruct H0. destruct H3.
  split.
  + eapply le_trans.
    - apply H.
    - assumption.
  + split.
    assumption.
    assumption.
Qed.

Proposition leC2_antisym: forall i j n:nat, 
  (leC2 i j n)-> (leC2 j i n) -> i=j.
Proof.
  intros.
  destruct H. 
  destruct H0.
  eapply le_antisymm.
  assumption.
  assumption.
Qed.

(*Mostremos que es un COTO reflexivo probando dicotomía*)

Proposition leC2_dichotomy: forall i j n:nat, 
  (Com2 i n)/\(Com2 j n)-> (leC2 i j n)\/ (leC2 j i n).
Proof.
  intros.
  destruct H.
  pose proof le_ge_cases.
  specialize H1 with (n:= i)(m:= j).
  destruct H1.
  + left.
    unfold leC2.
    split. assumption.
    split. assumption.
    assumption.
  + right.
    unfold leC2.
    split. assumption.
    split. assumption.
    assumption.
Qed.

(*Orden estricto en 2composiciones*)
Print leSC2.

(* Veamos que el orden tiene mínimo y máximo*)
Proposition leC2_Min: forall i n:nat,
  (Com2 i n) -> (leC2 0 i n).
Proof.
  intros.
  split.
  apply le_0_n.
  split.
  apply zero_Com2.
  assumption.
Qed.
  
  
Proposition leC2_Max: forall i n :nat, 
  (Com2 i n) -> (leC2 i n n).
Proof.
  intros.
  split.
  assumption.
  split. 
  assumption.
  constructor.
Qed.

(* INSERTAR PRUEBA DE QUE CUMPLE EL PRINCIPIO DEL BUEN ORDEN
    O BIEN QUE ES UN ORDEN INDUCTIVO*)
(* Falta mostrar que el orden es finito y al ser COBO, por el 
  teorema de la enumeración será isomorfo a un segmento inicial de 
  naturales. Esto planea hacer a futuro con la implementación de las
  funciones rank y Unrank que serán funciones biyectivas entre un segmento 
  inicial de naturales y las 2-composiciones de un número natural n
  Las funciones rank y Unrank serán una inversa de la otra.*)

(** 4. HasNext y Next *)

(*La función next cumple ser la función sucesor en este orden 
  y muestra la naturaleza iterativa de una composición, ya que al iterar
  sobre esta, se pueden generar todas las composiciones de un natural hasta
  agotar a las mismas.
  Algunos resultados que indican que la función 
  hasNext es sana respecto a la noción de ser una 
  2composición*)

Proposition hasNext2C_Com2: forall i n:nat, 
  hasNext2C i n -> Com2 i n.
Proof.
  intros.
  unfold Com2.
  unfold hasNext2C in H.
  pose proof le_succ_diag_r.
  specialize H0 with (n:= i).
  eapply le_trans.
  apply H0.
  assumption.
Qed.

Proposition next2C_Com2: forall i n: nat,
  hasNext2C i n -> Com2 (next2C i ) n.
Proof.
  intros.
  unfold next2C.
  unfold hasNext2C in H.
  unfold Com2.
  assumption.
Qed.

Proposition upbound_Com2: forall i j n:nat,
  Com2 i n->  j <= i -> Com2 j n.
Proof.
  intros. 
  unfold Com2 in *.
  eapply le_trans.
  apply H0.
  assumption.
Qed.

Proposition mono_next2C: forall i n :nat,
  Com2 i n-> hasNext2C i n-> leC2 i (next2C i) n.
Proof.
  intros.
  split.
  apply le_succ_diag_r.
  split.
  assumption.
  apply next2C_Com2.
  assumption.
Qed.

Proposition sandwT_next2C: forall i j n:nat, 
   hasNext2C i n -> 
    leC2 i j n -> leC2 j (next2C i) n ->
     ~i=j -> j = next2C i.
Proof.
  intros.
  unfold next2C.
  destruct H0.
  destruct H1.
  unfold next2C in H1.
  simpl in H1.

  
Admitted.
  
Proposition sandwB_next2C: forall i j n:nat, 
   hasNext2C i n -> 
    leC2 i j n -> leC2 j (next2C i) n ->
      j <> next2C i -> i=j.
Proof.
Admitted. 
     
