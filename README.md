# **Biblioteca de Composiciones de un número**
### **Proyecto Final de Verificación Formal PCIC**
### Autor: Luis Felipe Benítez Lluis LL11
En este repositorio se encuentras los archivos de coq correspondientes al proyecto para el curso de verificación formal. 
Se aboradram la propiedades básicas de las composiciones de un número n en k partes. 
## **Arquitectura**
El proyecto contiene tres carpetas cuya dependencia se maneja con un archivo *_CoqProject*. 
En la versión que poseo de coq el manejo de dependencias funcionó bien con el archivo 
en la forma en la que se encuentra. No obstante puede que este archivo precise modificaciones
mínimas, como incluirle los archivos en orden para su correcta portabilidad. 
Se compiló los archivo en el orden de dependencias provistas más adelante. 

### El proyecto contiene las siguientes carpetas:
1. **DosComposiciones**. Carpeta con los archivos .v donde se maneja la representación de dos composiciones de un número natural. Este script corresponde a la primera aproximación al problema correspondiente al proyecto medio. 
2. **TresComposiciones**. Carpeta con archivos .v correspondiente al manejo de 3-composiciones de un número. De igual forma estos scripts corresponden al proyecto medio. 

3. **kComposiciones**. Esta carpeta contiene todos los scripts correspondientes a la versión generalizada de composiciones de un número en k partes. Esta carpeta corresponde con el proyecto final y contiene las versiones mas actualizadas de manejo del problema. 


**Nota** Los manejos del problema para 	*DosComposiciones*	y *TresComposiciones* están ya algo obsoletos y por tanto puede con coincidan en nombre con las versiones en *kComposiciones*		

### **Contenidos por carpeta y lista de dependencias:**
Se agrega una lista de dependencias por cada carpeta. Se espera que se compilen de menor a mayor. 
* **DosComposiciones**: 
	1. *Defs_dosComp.v*
	2. *dosComp.v*
* **TresComposiciones**:
	1. *Defs_tresComp.v*
	2. *tresComp.v*
* **kComposiciones**: En el archivo *DocsKComp.txt* se pueden encontrar un resumen más claro sobre los contenidos de cada archivo en esta carpeta.
	1.	*Defs_des.v*
	2.	*Props_des.v* 
	3.	*Props_Orddes.v* 
	4.	*Defs_sep.v* 
	5.	*Props_sep.v* 
	6.	*Props_Ordsep.v*
	7.	*Defs_aux.v* 
	8.	*Props_aux.v*
	9.	*Defs_next.v* 
	10.	*Props_next.v*
	11.	*Defs_com.v*
	12.	*Props_com.v*
	13.	*Props_tranform.v*

Se complilaron los archivos con el uso de *CoqIDE* en el orden provisto. 

**Otros Archivos** En este repositorio se encuentra también el reporte de proyecto medio *ProyectoMedioVFLuisBLuis.pdf* junto con un archivo *DocsKComp.txt* donde viene un resumen más detallado de los contenidos por script de la carpeta *kComposiciones*.
